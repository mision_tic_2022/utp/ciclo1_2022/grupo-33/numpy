import numpy as np
from numpy import random as r

guion = lambda : print('-------------------------------------------------')

matriz = np.array( [ [10,20], [30,40] ] )
print(matriz)

guion()

matriz = np.array( [ [10,20], [30,40.2] ] )
print( matriz )

guion()
matriz = np.array( [ [10,20], [True, False] ] )
print(matriz)

guion()
matriz = np.array( [ [10, 20], [30.2, '40'] ] )
print(matriz)

guion()

matriz_1 = np.array( [ [10, 20, 30], [40, 50, 60] ] )
matriz_2 = np.array( [ [50, 60, 10], [90, 40, 60] ] )

guion()
print('Suma')
suma = matriz_1 + matriz_2
print(suma)

guion()
print('resta')
resta = matriz_1 - matriz_2
print(resta)

guion()
print('Multiplicar elemento a elemento')
multiplicar = matriz_1 * matriz_2
print(multiplicar)

guion()
matriz_1 = np.array( [ [2,4,1], [3,5,2] ] )
matriz_2 = np.array( [ [5,2], [1,4], [3,2] ] )

print('Multiplicación de matrices')
multiplicar = np.dot( matriz_1, matriz_2 )
print(multiplicar)

guion()
nombres = ['Sandy', 'Mateo', 'Roko', 'Milfer']

ganadores = r.choice( nombres, size=[2], p=[0.5, 0.1, 0.2, 0.2], replace=False )
print(ganadores)


